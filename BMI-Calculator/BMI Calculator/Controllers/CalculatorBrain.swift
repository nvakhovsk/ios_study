//
//  CalculatorBrain.swift
//  BMI Calculator
//
//  Created by Nataliia Vakhovska on 25.01.2021.
//

import UIKit

struct CalculatorBrain {
    var bmi: BMI?
    
    mutating func calculateBMI(height: Float, weight: Float) {
        let bmiValue = weight / pow(height, 2)
        if (bmiValue < 18.5) {
            bmi = BMI(value: bmiValue, advice: "Underweight", color: #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1))
        } else if (bmiValue > 24.9) {
            bmi = BMI(value: bmiValue, advice: "OverWeight", color: #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1))
        } else {
            bmi = BMI(value: bmiValue, advice: "Normal", color: #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1))
        }
    }
    func getBMIValue() -> String {
        return String(format: "%.1f", bmi?.value ?? "0.0")
    }
    func getAdvice() -> String {
        return bmi?.advice ?? "Error"
    }
    func getColor() -> UIColor {
        return bmi?.color ?? #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
}
