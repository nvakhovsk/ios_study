//
//  Sotry.swift
//  Destini-iOS13
//
//  Created by Nataliia Vakhovska on 18.01.2021.
//

import Foundation

struct Story {
    let title: String
    let choice1: String
    let choice1Destination: Int
    let choice2: String
    let choice2Destination: Int
}
