//
//  ViewController.swift
//  EggTimer
//
//  Created by Nataliia Vakhovska on 15/01/2021.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    let eggTimes = ["Soft": 1*60, "Medium": 2*60, "Hard": 3*60]
    var secondsPassed = 0
    var maxTime = 60
    var timer = Timer()
    var player: AVAudioPlayer!
    
    @IBOutlet weak var timerProgress: UIProgressView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBAction func hardnessSelected(_ sender: UIButton) {
        
        timer.invalidate()
        secondsPassed = 0
        
        let hardness = sender.currentTitle!
        titleLabel.text = hardness
        
        if (eggTimes.keys.contains(hardness)){
            maxTime = eggTimes[hardness]!
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        }
    }
    
    @objc func update() {
        if(secondsPassed <= maxTime) {
            print(secondsPassed)
            secondsPassed += 1
            timerProgress.progress = Float(secondsPassed) / Float(maxTime)
        }
        else {
            timer.invalidate()
            playSound(name: "alarm_sound")
            titleLabel.text = "DONE!"
        }
    }
    
    func playSound(name: String) {
        let url = Bundle.main.url(forResource: name, withExtension: "mp3")
        player = try! AVAudioPlayer(contentsOf: url!)
        player.play()
    }
    
}

