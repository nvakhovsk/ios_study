//
//  Question.swift
//  Quizzler-iOS13
//
//  Created by Nataliia Vakhovska on 17.01.2021.
//

import Foundation

struct Question {
    let text: String
    let answer: String
    init(q: String, a: String) {
        text = q
        answer = a
    }
}
