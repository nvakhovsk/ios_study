//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by Nataliia Vakhovska on 13/01/2021.
//

import UIKit

class ViewController: UIViewController {
    
    let ballArray = [#imageLiteral(resourceName: "ball1.png"),#imageLiteral(resourceName: "ball2.png"),#imageLiteral(resourceName: "ball3.png"),#imageLiteral(resourceName: "ball4.png"),#imageLiteral(resourceName: "ball5.png")]

    @IBOutlet weak var ballImg: UIImageView!
    

    @IBAction func askBtn(_ sender: UIButton) {
        ballImg.image = ballArray.randomElement()
    }
}

