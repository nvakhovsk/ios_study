//
//  ResultsViewController.swift
//  Tipsy
//
//  Created by Nataliia Vakhovska on 26.01.2021.
//

import UIKit

class ResultsViewController: UIViewController {
    
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var settingLabel: UILabel!
    
    var billPerPersone = 0.0
    var tipAmount = 0.0
    var splitNumber = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        totalLabel.text = String(format: "%.2f", billPerPersone)
        settingLabel.text = "Split between \(splitNumber) people, with \(Int(tipAmount*100))% tip."
    }
    
    @IBAction func recalculatePressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
