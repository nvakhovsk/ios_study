//
//  ViewController.swift
//  Tipsy
//
//  Created by Nataliia Vakhovska on 26.01.2021.
//

import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var billTextField: UITextField!
    @IBOutlet weak var zeroPctButton: UIButton!
    @IBOutlet weak var tenPctButton: UIButton!
    @IBOutlet weak var twentyPctButton: UIButton!
    @IBOutlet weak var splitNumberLabel: UILabel!
    
    var calculatorBrain = CalculatorBrain()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calculatorBrain.setTipAmount(1)
    }
    
    @IBAction func tipChanged(_ sender: UIButton) {
        billTextField.endEditing(true)
        zeroPctButton.isSelected = sender.tag == 0 ? true : false
        tenPctButton.isSelected = sender.tag == 1 ? true : false
        twentyPctButton.isSelected = sender.tag == 2 ? true : false
        
        calculatorBrain.setTipAmount(sender.tag)
    }
    
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        billTextField.endEditing(true)
        calculatorBrain.setSplitNumber(sender.value)
        splitNumberLabel.text = String(Int(sender.value))
    }
    
    @IBAction func calculatePressed(_ sender: UIButton) {
        calculatorBrain.setBill(billTextField.text ?? "0.0")
        calculatorBrain.calculateBillPerPerson()
        
        self.performSegue(withIdentifier: "goToResults", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "goToResults") {
            let destinationVC = segue.destination as! ResultsViewController
            destinationVC.tipAmount = calculatorBrain.getTip()
            destinationVC.splitNumber = calculatorBrain.getSplitNumber()
            destinationVC.billPerPersone = calculatorBrain.getBillPerPerson()
            
        }
    }
    
}

