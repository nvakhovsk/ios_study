//
//  CalculatorBrain.swift
//  Tipsy
//
//  Created by Nataliia Vakhovska on 26.01.2021.
//

import UIKit

struct CalculatorBrain {
    var tipAmount = 0.0
    var splitNumber = 1
    var bill = 0.0
    var billPerPerson = 0.0
    
    mutating func setTipAmount(_ tip: Int) {
        tipAmount = Double(tip) / Double(10)
    }
    
    mutating func setSplitNumber(_ n: Double) {
        splitNumber = Int(n)
    }
    
    mutating func setBill(_ value: String) {
        bill = Double(value) ?? 0.0
    }
    
    mutating func calculateBillPerPerson() {
        billPerPerson = (bill + (bill * tipAmount)) / Double(splitNumber)
    }
    
    func getBillPerPerson() -> Double {
        return billPerPerson
    }
    
    func getTip() -> Double {
        return tipAmount
    }
    
    func getSplitNumber() -> Int {
        return splitNumber
    }
    
}
